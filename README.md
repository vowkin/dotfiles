# dotfiles

These are my personal dotfiles, which I use on pretty much any GNU/Linux installation.

![Screenshot](/screenshot.png)

## Description

They are meant to be used on a somewhat minimal, tiling window manager-based system. The original idea comes from [Luke Smith's Voidrice](https://github.com/LukeSmithxyz/voidrice).

It includes settings for:

- [Alacritty](https://github.com/alacritty/alacritty) - A cross-platform, OpenGL terminal emulator
- [bspwm](https://github.com/baskerville/bspwm) - A tiling window manager based on binary space partitioning
- [Code - OSS](https://github.com/microsoft/vscode) - Visual Studio Code
- [dunst](https://github.com/dunst-project/dunst) - Lightweight and customizable notification daemon
- [lf](https://github.com/gokcehan/lf) - Terminal file manager
- [Neovim](https://github.com/neovim/neovim) - Vim-fork focused on extensibility and usability
- [picom](https://github.com/yshui/picom) - A lightweight compositor for X11
- [Polybar](https://github.com/polybar/polybar) - A fast and easy-to-use status bar
- [Powerlevel10k](https://github.com/romkatv/powerlevel10k) - A Zsh theme
- [Pywal](https://github.com/dylanaraps/pywal) - Generate and change color-schemes on the fly
- [Rofi](https://github.com/davatorium/rofi) - A window switcher, application launcher and dmenu replacement
- [sxhkd](https://github.com/baskerville/sxhkd) - Simple X hotkey daemon
- [sxiv](https://github.com/muennich/sxiv) - Simple X Image Viewer
- [xwallpaper](https://github.com/stoeckmann/xwallpaper) - Wallpaper setting utility for X
- [zathura](https://github.com/pwmt/zathura) - A document viewer
- [Zsh](https://zsh.sourceforge.io/) - Z shell

Some useful scripts are also included in [~/.local/bin](/.local/bin).
