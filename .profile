#!/bin/sh
#
# Profile file. Runs on login. Environmental variables are set here.

# Keyboard layout (localectl list-keymaps).
export KEYBOARD='es'

# Default network interface for Polybar
export DEFAULT_NETWORK_INTERFACE='ens33'

# Fix window and font rendering issues for Java applications.
export _JAVA_AWT_WM_NONREPARENTING=1
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'

# Default programs
export BROWSER='brave'
export EDITOR='nvim'
export FILE='lf'
# export PAGER='most'
export READER='zathura'
# export STATUSBAR="${LARBSWM}blocks"
export TERMINAL='alacritty'

# ~/ Clean-up
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export GOPATH="$XDG_DATA_HOME"/go
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
# export MOST_INITFILE="$XDG_CONFIG_HOME"/most/mostrc
export ZDOTDIR="$XDG_CONFIG_HOME"/zsh

# Other program settings
# export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --info=inline'

# Default 'less' arguments:
# -i turns on smartcase search
# -R allows ANSI color escape sequences
export LESS='-iR'

# Colored man pages
export LESS_TERMCAP_mb="$(printf '%b' '\e[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '\e[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '\e[0m')"
# export LESS_TERMCAP_so="$(printf '%b' '\e[01;44;33m')" # Yellow on blue
export LESS_TERMCAP_so="$(printf '%b' '\e[1;7;33m')"     # Bold, reverse yellow
export LESS_TERMCAP_se="$(printf '%b' '\e[0m')"
export LESS_TERMCAP_us="$(printf '%b' '\e[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '\e[0m')"

# Start graphical server on tty1 if not already running.
# Uncomment only when not using a display manager.
#[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx
