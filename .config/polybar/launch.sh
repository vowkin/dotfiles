#!/bin/sh
#
# Launch Polybar status bars.

# Bar(s) to launch, separated by spaces.
bars='main'

# Terminate already running bar instances.
polybar-msg cmd quit

# Define the interface for the Network module.
export DEFAULT_NETWORK_INTERFACE="$(ip route | grep '^default' | awk '{print $5}' | head -n1)"

for bar in $bars; do
  polybar -qr "$bar" 2>/dev/null &
done
