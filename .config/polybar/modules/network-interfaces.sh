#!/usr/bin/env bash
#
# Custom network interfaces script for Polybar (https://polybar.github.io).
#
# Author : Victor Lasa (vowkin)
# GitLab : https://gitlab.com/vowkin

readonly INTERFACES=(
  'tun0'
  'wlan0'
  'eth0'
  'enp0s3'
  # 'lo'
)

function getIP() {
  ip address show "$iface" | grep 'inet\b' | awk '{print $2}' | cut -d/ -f1
  # ip -br address show "$iface" | awk '{print $3}' | cut -d/ -f1
}

function notification() {
  echo "notify-send -u NORMAL -t 3000 '$NOTIFY_SUMM' '$NOTIFY_BODY'"
}

for iface in "${INTERFACES[@]}"; do
  if [ -d /sys/class/net/"$iface" ]; then
    if [ "$(cat /sys/class/net/"$iface"/operstate)" != 'down' ]; then
    # if [ "$(ip -br address show "$iface" | awk '{print $2}')" != 'DOWN' ]; then
      IP=$(getIP)
      # Ethernet (eth0)
      # if [ "$iface" = 'eth0' ] || [ "$iface" = 'enp0s3' ]; then
      if [[ "$iface" =~ ^(eth0|enp0s3)$ ]]; then
        ICON=''
        COLOR_ICON='%{F#55aa55}'
        COLOR_LABEL='%{F#e2ee6a}'
        NOTIFY_SUMM='Ethernet'
        NOTIFY_BODY="Connected to $iface"
      # Wireless (wlan0)
      elif [ "$iface" = 'wlan0' ]; then
        ICON='直'
        COLOR_ICON='%{F#55aa55}'
        COLOR_LABEL='%{F#e2ee6a}'
        NOTIFY_SUMM='Wireless'
        NOTIFY_BODY='Connected to wlan0'
      # Hack The Box (tun0)
      elif [ "$iface" = 'tun0' ]; then
        ICON=''
        COLOR_ICON='%{F#9acc14}'
        COLOR_LABEL='%{F#9acc14}'
        NOTIFY_SUMM='Hack The Box'
        NOTIFY_BODY='Connected to tun0'
      # Default values
      else
        ICON='爵'
        COLOR_ICON=''
        COLOR_LABEL=''
        NOTIFY_SUMM=''
        NOTIFY_BODY=''
      fi
      # Label only
      # echo -n "  $COLOR_ICON$ICON%{F-} $COLOR_LABEL$IP%{F-}"
      # Label + Notification
      echo -n " %{A1:$(notification):}$COLOR_ICON$ICON%{F-} $COLOR_LABEL$IP%{F-}%{A}"
    fi
  fi
done
