;----------------------------------------------------------------------;
; Network                                                              ;
; This module shows information about the specified network interface. ;
;----------------------------------------------------------------------;
[module/network]
type = internal/network

; Name of the network interface to display. You can get the names of the
; interfaces on your machine with `ip link`
; Wireless interfaces often start with `wl` and ethernet interface with `eno` or `eth`
;interface = ens33
interface = ${env:DEFAULT_NETWORK_INTERFACE:eth0}

; Seconds to sleep between updates
; Default: 1
interval = 1.0

; Accumulate values from all interfaces
; when querying for up/downspeed rate
; Default: false
accumulate-stats = true

; Consider an `UNKNOWN` interface state as up.
; Some devices like USB network adapters have 
; an unknown state, even when they're running
; Default: false
unknown-as-up = true

; Available tags:
;   <label-connected> (default)
;   <ramp-signal>
;format-connected = <ramp-signal> <label-connected>

; Available tags:
;   <label-disconnected> (default)
;format-disconnected = <label-disconnected>

; Available tags:
;   <label-connected> (default)
;   <label-packetloss>
;   <animation-packetloss>
;format-packetloss = <animation-packetloss> <label-connected>

; All labels support the following tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %local_ip6% [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]

; Default: %ifname% %local_ip%
format-connected-prefix = "%{T3}%{T-} "
format-connected-prefix-foreground = ${colors.secondary}
;label-connected = %{A1:"$(printf %local_ip% | xsel -b && notify-send -u low 'Ethernet (%ifname%)' 'Copied to clipboard')":}%local_ip%%{A}
label-connected = %{A1:"$(printf %local_ip% | xclip -r -f -selection clipboard && notify-send -u low 'Connected to %ifname%' 'IP has been copied to the clipboard')":}%local_ip%%{A}
label-connected-foreground = ${colors.foreground}

; Default: (none)
;format-disconnected-prefix = 
;label-disconnected = Disconnected!
;label-disconnected-foreground = #66ffffff
;label-disconnected-foreground = ${colors.primary}

; vim:ft=dosini
