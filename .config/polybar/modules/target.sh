#!/bin/sh
#
# Custom target script for Polybar (https://polybar.github.io).
#
# Author : Victor Lasa (vowkin)
# GitLab : https://gitlab.com/vowkin

notification() {
  printf '%s' "notify-send -u low -t 3000 '$NOTIFY_SUMM' '$NOTIFY_BODY'"
}

if [ -s '/dev/shm/.target' ]; then
  IP="$(cat /dev/shm/.target)"
  NOTIFY_SUMM="Target set to $IP"
  NOTIFY_BODY="It has been copied to the clipboard"
  # Label only
  # printf '%s' "$IP"
  # Label + Notification
  printf '%s' "%{A1:$(notification):}$IP%{A}"
else
  printf '\n'
fi
