"syntax on
"syntax enable
set encoding=utf-8
set termguicolors         " Enable true color support

"set visualbell
set number relativenumber " Show current and relative line numbers
set shortmess=I           " Hide intro message

set incsearch
set ignorecase
set smartcase
set smartindent
set nowrap
set scrolloff=10          " Lines to keep above and below the cursor

set cursorline
set splitbelow
set splitright

" Tab key enters 2 spaces
" To enter a TAB character when `expandtab` is in effect,
" CTRL-v-TAB
set expandtab tabstop=2 shiftwidth=2 softtabstop=2 " Tab key enters 2 spaces

" List mode configuration
set list                  " Enable list mode
set listchars=tab:→\      " Tab character
set listchars+=eol:↲      " End of line character
set listchars+=nbsp:␣     " Non-breakable space character
set listchars+=trail:•    " Trailing spaces
set listchars+=extends:⟩  " Last column character (nowrap)
set listchars+=precedes:⟨ " First visible column character
set showbreak=↪\          " Break character
match ErrorMsg '\s\+$'    " Color trailing spaces

set colorcolumn=80
" highlight colorcolumn ctermbg=9

set noshowmode " Hide current mode message (already shown in the statusline)

let mapleader = "\<Space>"

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | q | source $MYVIMRC | call lightline#update()
  \| endif

call plug#begin()
Plug 'tpope/vim-surround'
" Themes
Plug 'ntk148v/vim-horizon'
" Statusline
Plug 'itchyny/lightline.vim'
Plug 'voldikss/vim-floaterm'
"Plug 'mg979/vim-visual-multi', {'branch': 'master'}

Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
"Plug 'tpope/vim-fugitive'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
call plug#end()

""" Theme settings
colorscheme horizon
highlight Normal cterm=NONE ctermbg=233 ctermfg=252 gui=NONE guibg=NONE    guifg=NONE
highlight Pmenu  cterm=NONE ctermbg=233 ctermfg=252 gui=NONE guibg=#272C42 guifg=#e4e2ff

" https://github.com/RRethy/vim-hexokinase
let g:Hexokinase_highlighters = [ 'backgroundfull' ]
let g:Hexokinase_optInPatterns = [
  \  'full_hex',
  \  'triple_hex',
  \  'rgb',
  \  'rgba',
  \  'hsl',
  \  'hsla',
  \  'colour_names'
  \ ]

if filereadable(expand('$XDG_DATA_HOME/nvim/plugged/lightline.vim/plugin/lightline.vim'))
  let g:lightline = {
    \   'colorscheme': 'horizon',
    \   'active': {
    \     'left':[ [ 'mode', 'paste' ],
    \            [ 'gitbranch', 'readonly', 'filename', 'modified' ]
    \     ]
    \   },
    \   'component': {
    \     'lineinfo': '%3l:%-2v',
    \   },
    \   'component_function': {
    \     'gitbranch': 'fugitive#head',
    \   }
    \ }
  let g:lightline.separator = {
    \   'left': '', 'right': ''
    \}
  let g:lightline.subseparator = {
    \   'left': '', 'right': ''
    \ }

  let g:lightline.tabline = {
    \   'left': [ ['tabs'] ],
    \   'right': [ ['close'] ]
    \ }
endif
