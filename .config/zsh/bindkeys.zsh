#!/bin/sh
#
# Bindkeys.

bindkey               '^[[1;5D' backward-word         # ctrl-left
bindkey               '^[[1;5C' forward-word          # ctrl-right
bindkey               '^[[3~'   delete-char           # delete
bindkey               '^[[3;5~' delete-word           # ctrl-delete
bindkey               '^?'      backward-delete-char  # backspace
bindkey               '^H'      backward-delete-word  # ctrl-backspace
bindkey               '^[[H'    beginning-of-line     # home
bindkey               '^[[F'    end-of-line           # end
bindkey -M menuselect '^[[Z'    reverse-menu-complete # shift-tab
