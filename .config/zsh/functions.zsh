#!/bin/sh
#
# Useful functions.

# Swap two files.
sw() {
  tmpfile="$(mktemp "$(dirname "$1")"/XXXXXXXXXX)"
  mv "$1" "$tmpfile" && mv "$2" "$1" && mv "$tmpfile" "$2"
}

# Select a new wallpaper.
newbg() {
  printf "Press '%s' to set the selected wallpaper, '%s' to exit.\n" "$(tput bold)Ctrl-x + w$(tput sgr0)" "$(tput bold)q$(tput sgr0)"
  sxiv -to "$HOME/.local/share/wallpapers" 2>/dev/null
}

# Set target IP (shown on the status bar).
target() {
  if [ -n "$1" ]; then
    printf '%s' "$@" > /dev/shm/.target
  else
    rm -f /dev/shm/.target
  fi
}

# Generate a random password (32 characters long by default).
genpasswd() {
  l="$1"; [ -z "$l" ] && l=32
  tr -dc 'A-Za-z0-9!@#$%^&*' </dev/urandom | head -c "$l" | xargs
}

# More convenient port scanning.
scan() {
  ports="$(nmap -p- --min-rate=1000 -T4 "$1" | grep '^[0-9]' | cut -d '/' -f 1 | tr '\n' ',' | sed 's/,$//')"
  nmap -sC -sV -p "$ports" "$1"
}
