#!/bin/sh
#
# Aliases.

# System
alias sudo='sudo '                      # Allows the use of 'sudo' for aliases
alias c='clear'
alias q='exit'
alias ip='ip -color=auto'
# alias ls='ls --color=auto'
# alias dir='dir --color=auto'
# alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias em='emacs -nw'
alias dd='dd status=progress'

# LSD
alias l='lsd --group-dirs=first'
alias ls='lsd --group-dirs=first'       # Warning: can break some programs
alias ll='lsd -lh --group-dirs=first'
alias la='lsd -lhA --group-dirs=first'
alias lt='lsd -A --tree --group-dirs=first'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias json='python -m json.tool'
alias nse='ls /usr/share/nmap/scripts | grep '
alias htb="sudo openvpn ~/Documents/htb/vpn/vowkin.ovpn"
alias thm="sudo openvpn ~/Documents/thm/thm/vowkin.ovpn"
#alias cme='pushd /opt/CrackMapExec; pipenv shell; popd'
alias nessusd='sudo /etc/init.d/nessusd'
alias empire="sudo powershell-empire --rest --username empireadmin --password P@55word123"
alias covenant-start="sudo docker run -it -p 7443:7443 -p 80:80 -p 443:443 --name covenant -v /opt/Covenant/Covenant/Data:/app/Data covenant"
alias covenant='sudo docker start covenant -ai'
alias msf='sudo msfdb run'
