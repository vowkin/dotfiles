# Set background color from Pywal.
# printf '%b' "\033]11;$(head -n1 ${XDG_CACHE_HOME:-$HOME/.cache}/wal/colors)\007"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load Powerlevel10k
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ -f ~/.config/zsh/.p10k.zsh ]] && source ~/.config/zsh/.p10k.zsh

# History in cache directory:
HISTFILE="$HOME/.cache/zsh/history"
HISTSIZE=SAVEHIST=10000

setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt SHARE_HISTORY        # FIXME:
setopt INTERACTIVE_COMMENTS

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'   # Case-insensitive matching
zmodload zsh/complist
compinit
#_comp_options+=(globdots)          # Include hidden files

[ -f ~/.config/zsh/aliases.zsh ]   && source ~/.config/zsh/aliases.zsh
[ -f ~/.config/zsh/bindkeys.zsh ]  && source ~/.config/zsh/bindkeys.zsh
[ -f ~/.config/zsh/functions.zsh ] && source ~/.config/zsh/functions.zsh

# Switch to vim mode instantly.
export KEYTIMEOUT=1

# Change cursor shape for different vim modes.
zle-keymap-select() {
  if   [[ ${KEYMAP} == vicmd ]] ||
       [[ $1 = 'block' ]]; then
       echo -ne '\e[2 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
       echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

# Revert to blinking I-beam cursor when exiting vim.
zle-line-init() {
  zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
  echo -ne "\e[5 q"
}
zle -N zle-line-init

# Use lf to switch directories and bind it to ctrl-o
lfcd() {
  tmp="$(mktemp)"
  lf -last-dir-path="$tmp" "$@"
  if [ -f "$tmp" ]; then
    dir="$(cat "$tmp")"
    rm -f "$tmp"
    [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
  fi
}
bindkey -s '^o' 'lfcd\n'

################
# LOAD PLUGINS #
################

source ~/.config/zsh/plugins/calc/calc.plugin.zsh
source ~/.config/zsh/plugins/extract/extract.plugin.zsh
source ~/.config/zsh/plugins/sudo/sudo.plugin.zsh

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh

# Load zsh-syntax-highlighting; should be last.
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

####################
# PATH DEFINITIONS #
####################

path+=('/sbin')
path+=('/usr/sbin')
path+=("$HOME/.local/share/cargo/bin")
export PATH
